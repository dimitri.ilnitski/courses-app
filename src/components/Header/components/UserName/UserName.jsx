import './UserName.css';
import React from 'react';

function UserName() {
	return <div className='header__user-name'>Dave</div>;
}

export default UserName;
